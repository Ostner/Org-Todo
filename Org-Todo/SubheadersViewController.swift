//
//  SubheadersViewController.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/13/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class SubheadersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var headers: [Header]!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
    }

}

extension SubheadersViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return headers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "SubheaderCell", for: indexPath)
            as! HeaderCell
        cell.headerLabel.text = headers?[indexPath.row].title
        return cell
    }

}

extension SubheadersViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let subheaders = headers[indexPath.row].subheaders else { return }
        let destination = UIStoryboard.init(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "SubheadersViewController") as! SubheadersViewController
        destination.headers = subheaders
        show(destination, sender: self)

    }
}
