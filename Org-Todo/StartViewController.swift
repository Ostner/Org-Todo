//
//  StartViewController.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 12/29/16.
//  Copyright © 2016 Tobias Ostner. All rights reserved.
//

import UIKit
import SwiftyDropbox
import PromiseKit

class StartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var headers: [Header]? {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let _ = DropboxClientsManager.authorizedClient {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            spinner.startAnimating()
            firstly {
                    return DropboxHelper.promise(path: "/org/todo.org")
                }.then {
                    _, content in
                    return Parser.promise(string: content)
                }.then {
                    headers -> Void in
                    self.headers = headers
                }.always {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.spinner.stopAnimating()
            }
        } else {
            performSegue(
                withIdentifier: "authorizationViewController",
                sender: self)
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        guard let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell),
            let _ = headers?[indexPath.row].subheaders
        else { return false }
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSubheaders" {
            guard let destination = segue.destination as? SubheadersViewController else { fatalError("Wrong view controller") }
            guard let indexPath = tableView.indexPathForSelectedRow,
                let subheaders = headers?[indexPath.row].subheaders
            else { fatalError("No subheaders found") }
            destination.headers = subheaders
        }
    }
    
}

extension StartViewController {
    @IBAction func logout(_ sender: UIBarButtonItem) {
        DropboxClientsManager.unlinkClients()
    }
}


extension StartViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headers?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
        cell.textLabel?.text = headers?[indexPath.row].title
        return cell
    }

}
