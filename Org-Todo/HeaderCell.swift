//
//  HeaderCell.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/14/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    @IBOutlet weak var headerLabel: UILabel!
}
