//
//  AppDelegate.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 12/29/16.
//  Copyright © 2016 Tobias Ostner. All rights reserved.
//

import UIKit
import SwiftyDropbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        DropboxClientsManager.setupWithAppKey(appKey)
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        print("application open url")
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                print("Success! User is logged into Dropbox")
                let nav = window?.rootViewController as! UINavigationController
                nav.dismiss(animated: true, completion: nil)
            case .cancel:
                print("Authorization flow was manually cancelled by user!")
            case .error(_, let description):
                print("Error: \(description)")
            }
        }
        return false
    }

}

