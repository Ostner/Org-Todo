//
//  AuthorizationViewController.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/10/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import SwiftyDropbox

class AuthorizationViewController: UIViewController {

    @IBAction func authorize(_ sender: UIButton) {
        DropboxClientsManager.authorizeFromController(
            UIApplication.shared,
            controller: self,
            openURL: { url in
                print("authorize from controller completion handler")
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
        print("after authorize request")
    }
}
