//
//  DropboxHelper.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/11/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import SwiftyDropbox
import PromiseKit

struct DropboxHelper {

    typealias Metadata = Files.FileMetadataSerializer.ValueType
    
    static func promise(path: String) -> Promise<(Metadata, String)> {
        return Promise {
            fulfill, reject in
            DropboxClientsManager.authorizedClient?.files.download(path: path)
                .response {
                response, error in
                if let error = error {
                    let err = NSError(
                        domain: "DropboxHelper",
                        code: 0,
                        userInfo: [NSLocalizedDescriptionKey: error.description])
                    reject(err)
                }
                if let (metadata, data) = response,
                    let content = String(data: data, encoding: .utf8) {
                    fulfill((metadata, content))
                }
                else {
                    let err = NSError(
                        domain: "DropboxHelper",
                        code: 0,
                        userInfo: [NSLocalizedDescriptionKey: "Response has not the right format"])
                    reject(err)
                }
            }
        }
    }
}
