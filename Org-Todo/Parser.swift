//
//  Parser.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/2/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import PromiseKit

struct Header {
    let title: String
    var body: String?
    var status: String?
    var subheaders: [Header]?

    init(title: String, status: String? = nil, body: String? = nil, subheaders: [Header]? = nil) {
        self.title = title
        self.status = status
        self.body = body
        self.subheaders = subheaders
    }
}

enum LookAheadResult {
    case body
    case header(level: Int)
    case eof
}



extension Scanner {
    func scanUpTo(_ string: String) -> String? {
        var result: NSString?
        return scanUpTo(string, into: &result) ? (result as? String) : nil
    }

    func scanUpToCharacters(from set: CharacterSet) -> String? {
        var result: NSString?
        return scanUpToCharacters(from: set, into: &result) ? (result as? String) : nil
    }

    func scanCharacters(from set: CharacterSet) -> String? {
        var result: NSString?
        return scanCharacters(from: set, into: &result) ? (result as? String) : nil
    }

    func scanString(_ string: String) -> String? {
        var result: NSString?
        return scanString(string, into: &result) ? (result as? String) : nil
    }

}

class Parser {
    let string: String
    let scanner: Scanner
    let statusStrings = ["TODO", "DONE"]

    init(string: String) {
        self.string = string
        self.scanner = Scanner(string: string)
        self.scanner.charactersToBeSkipped = nil
    }

    static func promise(string: String) -> Promise<[Header]?> {
        return wrap(Parser(string: string).runAsync)
    }

    func runAsync(completion: @escaping ([Header]?) -> Void) {
        DispatchQueue.global().async {
            let headers = self.parseFile()
            completion(headers)
        }

    }

    func run() -> [Header]? {
        return parseFile()
    }
}

extension Parser {

    func parseFile() -> [Header]? {
        ignoreMetadata()
        var headers: [Header] = []
        while !scanner.isAtEnd {
            guard let header = parseHeader() else { return nil }
            headers.append(header)
        }
        return headers
    }

    func ignoreMetadata() {
        while !scanner.isAtEnd {
            let old = scanner.scanLocation
            if let _ = parseStars() {
                scanner.scanLocation = old
                return
            } else {
                _ = scanner.scanUpToCharacters(from: .newlines)
                _ = scanner.scanCharacters(from: .newlines)
            }
        }
    }

    func parseStars() -> Int? {
        guard let stars = scanner.scanCharacters(from: CharacterSet(charactersIn: "*")) else { return nil }
        guard let _ = scanner.scanCharacters(from: .whitespaces) else { return nil }
        return stars.characters.count
    }

    func parseStatus() -> String? {
        for status in statusStrings {
            if let _ = scanner.scanString(status), let _ = scanner.scanCharacters(from: .whitespaces) {
                return status
            }
        }
        return nil
    }

    func parseLeadingNewlines() -> String? {
        guard let newlines = scanner.scanCharacters(from: .newlines) else { return nil }
        let leadingView = newlines.characters.dropFirst()
        return String(leadingView)
    }

    func parseBody() -> String? {
        var body = ""
        while case .body = lookAhead() {
            let leadingNewlines = parseLeadingNewlines()
            let content = scanner.scanUpToCharacters(from: .newlines)
            let trailingNewlines = scanner.scanCharacters(from: .newlines)
            let result = (leadingNewlines ?? "") + (content ?? "") + (trailingNewlines ?? "")
            body.append(result)
        }
        return body.isEmpty ? nil : body
    }

    func parseSubheaders(level: Int) -> [Header]? {
        var subheaders: [Header] = []
        while case let .header(level: stars) = lookAhead(), level < stars {
            _ = scanner.scanCharacters(from: .newlines)
            guard let header = parseHeader() else { return nil }
            subheaders.append(header)
        }
        return subheaders.isEmpty ? nil : subheaders
    }

    func parseTitle() -> String? {
        guard let title = scanner.scanUpToCharacters(from: .newlines) else { return nil }
        return title
    }

    func parseHeader() -> Header? {
        _ = scanner.scanCharacters(from: .newlines)
        guard let stars = parseStars() else { return nil }
        let status = parseStatus()
        guard let title = parseTitle() else { return nil }
        let body = parseBody()
        let subheaders = parseSubheaders(level: stars)
        return Header(title: title, status: status, body: body, subheaders: subheaders)
    }

    func lookAhead() -> LookAheadResult {
        guard !scanner.isAtEnd else { return .eof }
        let old = scanner.scanLocation
        _ = scanner.scanCharacters(from: .newlines)
        let result: LookAheadResult
        if let stars = parseStars() {
            result = .header(level: stars)
        } else {
            result = .body
        }
        scanner.scanLocation = old
        return result
    }

}
