//
//  ParserTests.swift
//  Org-Todo
//
//  Created by Tobias Ostner on 1/6/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import XCTest

class ParserTests: XCTestCase {

    func testParseEmptyFile() {
        let string = ""
        let parser = Parser(string: string)
        let result = parser.run()
        XCTAssertNotNil(result)
        XCTAssertTrue(result!.isEmpty)
    }

    func testParseStars() {
        let string = "** "
        let parser = Parser(string: string)
        XCTAssertEqual(parser.parseStars(), 2)
    }

    func testParseStars_emptyString() {
        let string = ""
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseStars())
    }

    func testParseStars_notAtBeginning() {
        let string = "  *  "
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseStars())
    }

    func testParseStatus() {
        let string1 = "TODO "
        let parser1 = Parser(string: string1)
        let result1 = parser1.parseStatus()
        XCTAssertEqual(result1!, "TODO")
        let string2 = "DONE "
        let parser2 = Parser(string: string2)
        let result2 = parser2.parseStatus()
        XCTAssertEqual(result2!, "DONE")
    }

    func testParseStatus_noTrailingSpaces() {
        let string = "TODO"
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseStatus())
    }

    func testParseBody() {
        let string = "\n  Body"
        let parser = Parser(string: string)
        let result = parser.parseBody()
        XCTAssertNotNil(result)
        XCTAssertEqual(result!, "  Body")
    }

    func testParseBody_trailingNewlines() {
        let string = "\nBody\n\n"
        let parser = Parser(string: string)
        let result = parser.parseBody()
        XCTAssertNotNil(result)
        XCTAssertEqual(result!, "Body\n\n")
    }

    func testParseBody_onlyNewlines() {
        let string = "\n\n"
        let parser = Parser(string: string)
        let result = parser.parseBody()
        XCTAssertNotNil(result)
        XCTAssertEqual(result!, "\n")
    }

    func testLookAhead_scanLocation() {
        let string = "**"
        let parser = Parser(string: string)
        let old = parser.scanner.scanLocation
        _ = parser.lookAhead()
        XCTAssertEqual(parser.scanner.scanLocation, old)
    }

    func testLookAhead_body() {
        let string = "\nBody"
        let parser = Parser(string: string)
        XCTAssertEqual(parser.lookAhead(), LookAheadResult.body)
    }

    func testLookAhead_header() {
        let string = "\n* "
        let parser = Parser(string: string)
        let result = parser.lookAhead()
        XCTAssertEqual(result, LookAheadResult.header(level: 1))
    }

    func testLookAhead_newlines() {
        let string = "\n\n"
        let parser = Parser(string: string)
        let result = parser.lookAhead()
        XCTAssertEqual(result, LookAheadResult.body)
        XCTAssertEqual(parser.scanner.scanLocation, 0)
    }

    func testParseHeader_onlyTitle() {
        let string = "* Header"
        let parser = Parser(string: string)
        let result = parser.parseHeader()
        XCTAssertNotNil(result)
        XCTAssertEqual(result!.title, "Header")
    }

    func testParseHeader_emptyTitle() {
        let string = "* "
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseHeader())
    }

    func testParseHeader_whitespacesTitle() {
        let string = "*        "
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseHeader())
    }

    func testParserHeader_noSpace() {
        let string = "*Header"
        let parser = Parser(string: string)
        XCTAssertNil(parser.parseHeader())
    }

    func testParseHeader_statusTodo() {
        let string = "* TODO Header"
        let parser = Parser(string: string)
        let result = parser.parseHeader()
        XCTAssertNotNil(result)
        XCTAssertNotNil(result!.status)
        XCTAssertEqual(result!.status, "TODO")
    }

    func testParserHeader_statusDone() {
        let string = "* DONE Header"
        let parser = Parser(string: string)
        let result = parser.parseHeader()
        XCTAssertNotNil(result)
        XCTAssertNotNil(result!.status)
        XCTAssertEqual(result!.status, "DONE")
    }

    func testParseHeader_onlyBody() {
        let string = "* Header\n\nBody"
        let parser = Parser(string: string)
        let result = parser.parseHeader()!
        XCTAssertEqual(result.body!, "\nBody")
    }

    func testParseSubheaders() {
        let string = "* H1\n** H1.1"
        let parser = Parser(string: string)
        let result = parser.parseHeader()!
        XCTAssertEqual(result.title, "H1")
        XCTAssertEqual(result.subheaders!.first!.title, "H1.1")
    }

    func testParseSubheaders_two() {
        let string = "* H1\n** H1.1\n** H1.2"
        let parser = Parser(string: string)
        let result = parser.parseHeader()!
        XCTAssertEqual(result.subheaders!.count, 2)
    }

    func testParseSubheaders_bodyAndTwo() {
        let string = "* H1\nBody\n** H1.1\n** H1.2"
        let parser = Parser(string: string)
        let result = parser.parseHeader()!
        XCTAssertNotNil(result.body)
        XCTAssertEqual(result.subheaders!.count, 2)
    }

    func testParseFile_onlyHeader() {
        let string = "* Header"
        let parser = Parser(string: string)
        let result = parser.parseFile()!
        XCTAssertEqual(result.first!.title, "Header")
    }

    func testParseFile_withMetaData() {
        let string = "#+TITLE: Title\n\n* Header"
        let parser = Parser(string: string)
        let result = parser.parseFile()!
        XCTAssertEqual(result.first!.title, "Header")
    }

    func testParseFile_MetadataHeaderSubheaderHeader() {
        let string = "#+TITLE: Title\n\n* H1\n** H1.1\n* H2"
        let parser = Parser(string: string)
        let result = parser.parseFile()!
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first!.subheaders!.count, 1)
    }

    
}


extension LookAheadResult: Equatable {
    static func ==(lhs: LookAheadResult, rhs: LookAheadResult) -> Bool {
        switch (lhs, rhs) {
        case (.body, .body): return true
        case (.header(level: let l1), .header(level: let l2)) where l1 == l2: return true
        default: return false
        }
    }
}
